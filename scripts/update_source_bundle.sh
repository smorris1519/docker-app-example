#!/bin/bash

# Create new Dockerrun file with updated image tag
cp $PROD_S3KEY $REVIEW_S3KEY
sed -i "s/latest/$CI_COMMIT_REF_SLUG/" $REVIEW_S3KEY
aws s3 cp $REVIEW_S3KEY s3://$S3BUCKET