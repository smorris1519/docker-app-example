#!/bin/bash

APPLICATION=$(aws elasticbeanstalk describe-applications --application-names $REVIEW_APPNAME --query "Applications[*]" --output text)
# if application does not exist, create an application
if [ -z "$APPLICATION" ]; then
    echo 'Application does not exist. Creating application.'
    aws elasticbeanstalk create-application --application-name $REVIEW_APPNAME
fi

echo 'Creating application version.'
aws elasticbeanstalk create-application-version --application-name $REVIEW_APPNAME --version-label $REVIEW_LABEL --source-bundle S3Bucket=$S3BUCKET,S3Key=$REVIEW_S3KEY

ENVIRONMENT=$(aws elasticbeanstalk describe-environments --environment-names $REVIEW_ENVNAME --query "Environments[*]" --output text)
# if environment does not exist, create an environment
if [ -z "$ENVIRONMENT" ]; then
    echo 'Environment does not exist. Creating environment with application version.'
    aws elasticbeanstalk create-environment --application-name $REVIEW_APPNAME --version-label $REVIEW_LABEL --environment-name $REVIEW_ENVNAME --solution-stack-name "64bit Amazon Linux 2023 v4.0.0 running Docker" --option-settings file://options.txt
else
   echo 'Environment exists. Updating environment with application version.'
   aws elasticbeanstalk update-environment --application-name $REVIEW_APPNAME --environment-name $REVIEW_ENVNAME --version-label $REVIEW_LABEL
fi
