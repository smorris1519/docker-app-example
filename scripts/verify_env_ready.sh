#!/bin/bash

DYNAMIC_ENVIRONMENT_URL=$(aws elasticbeanstalk describe-environments --environment-names $REVIEW_ENVNAME --query "Environments[*].EndpointURL" --output text)
if [ -z "$DYNAMIC_ENVIRONMENT_URL" ]; then
    exit 1
else
    exit 0
fi