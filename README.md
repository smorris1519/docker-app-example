# Welcome!

This project features a website built from a Docker image and deployed via GitLab CI/CD. The Docker image is built, then it is uploaded to the Container Registry, and the container scanning pulls the built image and conducts scans. Finally, AWS Elastic Beanstalk pulls in the latest Docker image from the container registry and updates the application with the new Docker image using the Dockerrunaws.json file and registry configuration in the config.json file stored in S3.

This also contains a manual deployment job with deployment approval rules configured, and a release creation process upon merge to the main branch.

You can access the site [here on Elastic Beanstalk](http://docker-example-env.eba-fry3mu25.us-east-2.elasticbeanstalk.com/).

Code derived from this [tutorial](https://docker-curriculum.com/).

AWS integration derived from this [tutorial](https://medium.com/devops-with-valentine/gitlab-ci-how-to-deploy-a-docker-image-to-aws-elastic-beanstalk-from-a-private-docker-registry-227582f57917).

Steps to reproduce:
- Create an AWS account
- Create an Elastic Beanstalk Application and Environment (also create an EC2 instance role as the default settings for creating an environment does not create this).
- Create a config.json and Dockerrunaws.json and add them to an S3 bucket.
- Create a deploy token with read repo and read registry permissions and convert it to base64 before adding it to the config.json. Command is: `echo "{DEPLOY_TOKEN}" | tr -d "\n" | base64`

# Release Notes
Updating main page text with a new DevOps post.