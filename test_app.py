import unittest
from unittest.mock import patch
from app import app

class TestApp(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_index_route(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'<img src="', response.data)

    @patch('app.random.choice')
    def test_random_image_selection(self, mock_choice):
        mock_choice.return_value = 'https://example.com/cat.gif'
        response = self.app.get('/')
        self.assertIn(b'https://example.com/cat.gif', response.data)

    def test_images_list(self):
        from app import images
        self.assertIsInstance(images, list)
        self.assertGreater(len(images), 0)
        for image_url in images:
            self.assertIsInstance(image_url, str)
            self.assertTrue(image_url.startswith('https://'))

if __name__ == '__main__':
    unittest.main()